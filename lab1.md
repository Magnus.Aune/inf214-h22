# INF214 H22 - Lab 1 - JVM languages - for week 36


## Task 1
Assume we have variables `x`, `y` and `z`, which all are initialized to 0.
Suppose we have two threads which are as follows.
What are the possible values of variable `x` after running the threads concurrently?
Explain your answer.


```java
Thread t1 = new Thread() {
    public void run() {
        x = y + z;
    }
};

Thread t2 = new Thread() {
    public void run() {
        y = 1;
        z = 2;
    }
};
```

---

*Use a JVM-language (for example, Java, Kotlin, Scala, ...) in the tasks 2-4 below.*

## Task 2
Implement method `run_both`, which takes two functions as parameters, and calls each of them in a new thread.
The method must return a tuple with the result of the both functions.

## Task 3
Implement method `periodically`, which takes two parameters:
- a time interval `duration` (specified in milliseconds), and
- a function `f`.

The method `periodically` starts a thread that calls the function `f` every `duration` milliseconds.

## Task 4
Implement a function that computes the sum of two matrices so that all *rows* are summed concurrently.


